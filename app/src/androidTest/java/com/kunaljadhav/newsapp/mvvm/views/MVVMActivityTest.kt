package com.kunaljadhav.newsapp.mvvm.views

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.IdlingRegistry
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.kunaljadhav.newsapp.IdlingResource
import com.kunaljadhav.newsapp.R
import com.kunaljadhav.newsapp.screens.ActivityScreen
import com.kunaljadhav.newsapp.view.adapters.NewslistAdapter
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MVVMActivityTest{
    @Rule
    @JvmField
    val rule = ActivityTestRule(MVVMActivity::class.java)
    lateinit var screen: ActivityScreen

    @Before
    fun setup() {
        screen = ActivityScreen()
        IdlingRegistry.getInstance().register(IdlingResource.getIdlingResource()); // make Espresso aware of asynchronous tasks, to wait before calling ViewMatchers
    }

    /**
     *  Test validates the following-
     *  ->check if 24 items are inflated in recyclerView @rv_news of NewslistAdapter
     *  ->Scroll to last item and check if TextView @news_title of @rv_news row is displayed, perform click operation
     *  ->Perform click operation on @fullStoryLinkButton of NewsDetailFragment
     */
    @Test
    fun test() {
        screen {
            rv_news {
                isVisible()
                hasSize(24)
                scrollTo(23)
                childAt<ActivityScreen.Item>(23, {
                    isVisible()
                    news_title {
                        isDisplayed()
                        click()
                    }
                })
            }
            fullStoryLinkButton {
                click()
            }
        }
        onView(withId(R.id.recycler)).perform(RecyclerViewActions.actionOnItemAtPosition<NewslistAdapter.MyViewHolder>(0, click()));

    }


    @After
    fun unregisterIdlingResource() {
        IdlingRegistry.getInstance().unregister(IdlingResource.getIdlingResource())
    }
}