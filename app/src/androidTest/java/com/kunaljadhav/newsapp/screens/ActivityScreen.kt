package com.kunaljadhav.newsapp.screens

import android.view.View
import com.agoda.kakao.*
import com.kunaljadhav.newsapp.R
import org.hamcrest.Matcher

class ActivityScreen: Screen<ActivityScreen>() {
    val rv_news: KRecyclerView = KRecyclerView({
        withId(R.id.recycler)
    }, itemTypeBuilder = {
        itemType(::Item)
    })

    val fullStoryLinkButton: KButton = KButton{
        withId(R.id.full_story_link)
    }


    class Item(parent: Matcher<View>) : KRecyclerItem<Item>(parent) {
        val news_title: KTextView = KTextView(parent) { withId(R.id.news_title) }
        val news_item_image: KImageView = KImageView(parent) { withId(R.id.news_item_image) }
    }



}