package com.kunaljadhav.newsapp

import com.kunaljadhav.newsapp.model.NewsDataResponse
import com.kunaljadhav.newsapp.model.Result
import com.kunaljadhav.newsapp.mvp.contracts.NewsListContract
import com.kunaljadhav.newsapp.mvp.presenters.NewsListPresenter
import com.kunaljadhav.newsapp.retrofit.APIInterface
import com.kunaljadhav.newsapp.room.ResultPojo
import com.kunaljadhav.newsapp.utils.Constants_Kot
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class NewsListPresenterTest {

    @Mock
    lateinit var view: NewsListContract.view
    @Mock
    lateinit var apiInterface: APIInterface

    lateinit var newsDataResponse: NewsDataResponse

    lateinit var newsListPresenter: NewsListPresenter

    private var  testScheduler= TestScheduler() // Mock schedulers using RxJava TestScheduler.


    companion object {
        @BeforeClass
        @JvmStatic
        fun beforeClass() {}
        @AfterClass
        @JvmStatic
        fun afterClass() {}
    }


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        newsListPresenter = NewsListPresenter()
        newsListPresenter.setSchedulers(testScheduler,testScheduler)
    }

    @Test
    fun checkgetNewsDataApiSuccess() {
        newsListPresenter.attachView(view)
        var result = Result(null, null, null, null, null, null, null, null,
                null, null, null, null, null, null, null, null, null)
        newsDataResponse = NewsDataResponse(null, null, null, listOf(result), null, Constants_Kot.statusOK)
        Mockito.`when`(apiInterface.getNewsData())
                .thenReturn(Single.just(newsDataResponse))
        newsListPresenter.callNewsListApi(apiInterface)
        testScheduler.triggerActions()
        //  val inOrder = Mockito.inOrder(view) // inOrder is used to check the number of invocations on a method
        //  inOrder.verify(view, Mockito.times(1)).loadNewsData(newsDataResponse.results as List<Result>?) // check on object 'view', whether its loadNewsData() is invoked 1 time
        val resultPojoList = arrayListOf(ResultPojo(0, result.title,
                result.multimedia?.get(0)?.url?.let { it },
                result.abstract, result.multimedia?.let { it.get(2)?.let { it.url?.let { it } } },
                result.url))
        Mockito.verify(view).loadNewsData(resultPojoList)
        Mockito.verify(view, Mockito.never()).onError()
    }

    @Test
    fun checkgetNewsDataApi_withResult_null() {
        newsListPresenter.attachView(view)
        newsDataResponse = NewsDataResponse(null, null, null, null, null, Constants_Kot.statusOK)
        Mockito.`when`(apiInterface.getNewsData())
                .thenReturn(Single.just(newsDataResponse))
        newsListPresenter.callNewsListApi(apiInterface)
        testScheduler.triggerActions()
        Mockito.verify(view).onError()
        Mockito.verify(view, Mockito.never()).loadNewsData(null)

    }


    @Test
    fun callgetNewsDataApi_withError() {
        newsListPresenter.attachView(view)
        newsDataResponse = NewsDataResponse(null, null, null, null, null, Constants_Kot.statusERROR)
        Mockito.`when`(apiInterface.getNewsData())
                .thenReturn(Single.just(newsDataResponse))
        newsListPresenter.callNewsListApi(apiInterface)
        testScheduler.triggerActions()
        Mockito.verify(view).onError()
        Mockito.verify(view, Mockito.never()).loadNewsData(null)
    }

    @Test
    fun callgetNewsDataApi_withException() {
        newsListPresenter.attachView(view)
        Mockito.`when`(apiInterface.getNewsData())
                .thenReturn(Single.error(Exception()))
        newsListPresenter.callNewsListApi(apiInterface)
        testScheduler.triggerActions()
        Mockito.verify(view).onError()
    }
}