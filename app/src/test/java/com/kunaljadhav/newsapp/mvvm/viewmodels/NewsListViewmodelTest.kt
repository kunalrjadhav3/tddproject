package com.kunaljadhav.newsapp.mvvm.viewmodels

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.kunaljadhav.newsapp.model.NewsDataResponse
import com.kunaljadhav.newsapp.model.Result
import com.kunaljadhav.newsapp.retrofit.APIInterface
import com.kunaljadhav.newsapp.room.AppDatabase
import com.kunaljadhav.newsapp.room.ResultPojo
import com.kunaljadhav.newsapp.utils.Constants_Kot
import com.kunaljadhav.newsapp.utils.NetworkStatus
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.*
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations


class NewsListViewmodelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    @Mock
    lateinit var apiInterface: APIInterface
    @Mock
    lateinit var appDatabase: AppDatabase


    lateinit var dataObserver: Observer<List<ResultPojo>>
    lateinit var networkStatusObserver: Observer<NetworkStatus>
    lateinit var newsListViewmodel: NewsListViewmodel
    lateinit var newsDataResponse: NewsDataResponse
    private var testScheduler = TestScheduler() // Mock schedulers using RxJava TestScheduler.

    companion object {
        @BeforeClass
        @JvmStatic
        fun beforeClass() {
        }

        @AfterClass
        @JvmStatic
        fun afterClass() {
        }
    }


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        newsListViewmodel = NewsListViewmodel()

    }

    @Test
    fun checkgetNewsDataApiSuccess() {
        var result = Result(null, null, null, null, null, null, null, null,
                null, null, null, null, null, null, null, null, null)
        newsDataResponse = NewsDataResponse(null, null, null, listOf(result), null, Constants_Kot.statusOK)
        Mockito.`when`(apiInterface.getNewsData())
                .thenReturn(Single.just(newsDataResponse))

        dataObserver = mock(Observer::class.java) as Observer<List<ResultPojo>>
        newsListViewmodel.setApiInterfaceInstance(apiInterface) //set mocked Api interface
        newsListViewmodel.resultListLiveData.observeForever(dataObserver) // register mocked Observer with resultListLiveData
        newsListViewmodel.callNewtWorkRepository(testScheduler, testScheduler) // invoke network call
        testScheduler.triggerActions()
        Assert.assertNotNull(newsListViewmodel.resultListLiveData.value) // check is resultListLiveData value is set

        val resultPojoList = arrayListOf(ResultPojo(0, result.title,
                result.multimedia?.get(0)?.url?.let { it },
                result.abstract, result.multimedia?.let { it.get(2)?.let { it.url?.let { it } } },
                result.url))
        Mockito.verify(dataObserver).onChanged(resultPojoList) // check if mocked Observer's onChanged() is called

    }

    @Test
    fun checkgetNewsDataApi_withResult_null(){
        newsDataResponse = NewsDataResponse(null, null, null, null, null, Constants_Kot.statusOK)
        Mockito.`when`(apiInterface.getNewsData())
                .thenReturn(Single.just(newsDataResponse))

        networkStatusObserver = mock(Observer::class.java) as Observer<NetworkStatus>
        newsListViewmodel.setApiInterfaceInstance(apiInterface) //set mocked Api interface
       // newsListViewmodel.NetworkStatusLiveData.observeForever(networkStatusObserver) // register mocked Observer with resultListLiveData
        newsListViewmodel.callNewtWorkRepository(testScheduler, testScheduler) // invoke network call
        testScheduler.triggerActions()
        Assert.assertNull(newsListViewmodel.resultListLiveData.value) // check is resultListLiveData value is null
        Assert.assertEquals(Constants_Kot.error_noData,(newsListViewmodel.NetworkStatusLiveData.value as NetworkStatus.Error).message)

    }

    @Test
    fun callgetNewsDataApi_withError(){
        newsDataResponse = NewsDataResponse(null, null, null, null, null, Constants_Kot.statusERROR)
        Mockito.`when`(apiInterface.getNewsData())
                .thenReturn(Single.just(newsDataResponse))
        newsListViewmodel.setApiInterfaceInstance(apiInterface)
        newsListViewmodel.callNewtWorkRepository(testScheduler,testScheduler)
        testScheduler.triggerActions()
        Assert.assertNull(newsListViewmodel.resultListLiveData.value) // check is resultListLiveData value is null
        Assert.assertEquals(Constants_Kot.error_noData,(newsListViewmodel.NetworkStatusLiveData.value as NetworkStatus.Error).message)

    }

    @Test
    fun callgetNewsDataApi_withException(){
        Mockito.`when`(apiInterface.getNewsData())
                .thenReturn(Single.error(Exception()))
        newsListViewmodel.setApiInterfaceInstance(apiInterface)
        newsListViewmodel.callNewtWorkRepository(testScheduler,testScheduler)
        testScheduler.triggerActions()
        Assert.assertNull(newsListViewmodel.resultListLiveData.value) // check is resultListLiveData value is null
        Assert.assertEquals("java.lang.Exception",(newsListViewmodel.NetworkStatusLiveData.value as NetworkStatus.Exception).exception.toString())
    }


}