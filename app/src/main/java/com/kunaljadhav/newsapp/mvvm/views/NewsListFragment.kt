package com.kunaljadhav.newsapp.mvvm.views

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kunaljadhav.newsapp.MainApp
import com.kunaljadhav.newsapp.R
import com.kunaljadhav.newsapp.di.components.DaggerFragmentComponent
import com.kunaljadhav.newsapp.di.components.FragmentComponent
import com.kunaljadhav.newsapp.di.modules.FragmentModule
import com.kunaljadhav.newsapp.mvvm.viewmodels.NewsListViewmodel
import com.kunaljadhav.newsapp.retrofit.APIInterface
import com.kunaljadhav.newsapp.room.AppDatabase
import com.kunaljadhav.newsapp.room.ResultPojo
import com.kunaljadhav.newsapp.utils.NetworkStatus
import com.kunaljadhav.newsapp.utils.Utility
import com.kunaljadhav.newsapp.view.adapters.NewslistAdapter
import com.kunaljadhav.newsapp.view.fragments.BaseFragment
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_news_list.*
import javax.inject.Inject


class NewsListFragment : BaseFragment() {

    private var fragmentComponent: FragmentComponent? = null
    val newsListViewmodel by lazy { ViewModelProviders.of(activity!!).get(NewsListViewmodel::class.java) }
    @Inject
    lateinit var apiInterface: APIInterface
    @Inject
    lateinit var appDatabase: AppDatabase
    private lateinit var callback: Callback


    /**
     * @Callback interface for data communication with @MainActivity
     */
    interface Callback {
        fun activityCallback()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MVVMActivity) {
            callback = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDependency()
        newsListViewmodel.setApiInterfaceInstance(apiInterface).setDBInstance(appDatabase)
        setViewObservers()


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_news_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        newsListViewmodel.resultListLiveData.value?.let { initRecyclerView(it as ArrayList<ResultPojo>) }
    }

    private fun initRecyclerView(resultList: ArrayList<ResultPojo>) {
        var adapter = NewslistAdapter(resultList, object : NewslistAdapter.ItemClickListener {
            override fun onitemClick(result: ResultPojo) {
                newsListViewmodel.resultLiveData.value = result
                callback.activityCallback()
            }
        })

        var mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(getContext())
        recycler.setLayoutManager(mLayoutManager)
        recycler.setItemAnimator(DefaultItemAnimator())
        recycler.setAdapter(adapter)
    }

    fun onError(errorMessage: String) {
        Utility.displaySnackbar(view!!, errorMessage)
    }

    private fun setViewObservers() {
        newsListViewmodel.resultListLiveData.observe(this, Observer {
            initRecyclerView(it!! as ArrayList<ResultPojo>)
        })
        newsListViewmodel.NetworkStatusLiveData.observe(this, Observer {
            when (it) {
                is NetworkStatus.Loading ->
                    setProgressBarVisibility(true)

                is NetworkStatus.Loaded -> {
                    setProgressBarVisibility(false)

                        if (newsListViewmodel.getDataFromDB(appDatabase).equals(false))
                            newsListViewmodel.insertDatatoDB(newsListViewmodel.resultListLiveData.value)
                 }
                is NetworkStatus.Error -> {
                    setProgressBarVisibility(false)
                    onError(it.message)
                }
                is NetworkStatus.Exception -> {
                    setProgressBarVisibility(false)
                    onError(it.exception.localizedMessage)
                }
            }
        })
    }

    private fun setProgressBarVisibility(show: Boolean) {
        if (show) progressBar.visibility = View.VISIBLE else progressBar.visibility = View.GONE

    }

    override fun checkInternetconnect(isConnected: Boolean) {
        when (isConnected) {
            true -> {
                // if internet available and data not loaded, call API
                if (newsListViewmodel.resultListLiveData.value == null) newsListViewmodel.callNewtWorkRepository()
            }
            false -> {
              //  GlobalScope.launch {
                    //if internet unavailable, no data loaded to UI, no data not SQLite, show message
                    if (newsListViewmodel.resultListLiveData.value == null && newsListViewmodel.getDataFromDB(appDatabase).equals(false))
                        Utility.displaySnackbar(view!!, getString(R.string.noInternet_msg))

                    //if internet unavailable, no data loaded to UI, data available in SQLite, load data
                    if (newsListViewmodel.resultListLiveData.value == null && newsListViewmodel.getDataFromDB(appDatabase).equals(true))
                        Utility.displaySnackbar(view!!, getString(R.string.offlineData_msg))

              //  }
            }
        }
    }

    /**
     * initialize dependencies with @CustomScope
     */
    private fun initDependency() {
        fragmentComponent = DaggerFragmentComponent.builder().applicationComponent(MainApp.applicationComponent)
                .fragmentModule(FragmentModule())
                .build()
        fragmentComponent!!.inject(this)


    }


    internal fun registerStringObservabletoObserver(stringObservable: Observable<String>) {
        newsListViewmodel.resgisterSearchStringSubscription(stringObservable).observeOn(AndroidSchedulers.mainThread()).subscribe(getObserver())
    }

    private fun getObserver(): io.reactivex.Observer<List<ResultPojo>> {
        return object : io.reactivex.Observer<List<ResultPojo>> {
            override fun onComplete() {}
            override fun onError(e: Throwable) {}
            override fun onNext(searchResultList: List<ResultPojo>) {
                if (recycler.adapter is NewslistAdapter) {
                    val adapterInstance = recycler.adapter as NewslistAdapter
                    adapterInstance.resultList.clear()
                    adapterInstance.resultList.addAll(searchResultList)
                    adapterInstance.notifyDataSetChanged()
                }
            }

            override fun onSubscribe(d: Disposable) {}
        }

    }






    override fun onDestroy() {
        super.onDestroy()
        fragmentComponent = null // set @CustomScope dependencies to null
    }
}