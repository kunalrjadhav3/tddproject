package com.kunaljadhav.newsapp.mvvm.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.kunaljadhav.newsapp.repository.DBRepository
import com.kunaljadhav.newsapp.repository.NetworkRepository
import com.kunaljadhav.newsapp.retrofit.APIInterface
import com.kunaljadhav.newsapp.room.AppDatabase
import com.kunaljadhav.newsapp.room.ResultPojo
import com.kunaljadhav.newsapp.utils.NetworkStatus
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class NewsListViewmodel : ViewModel() {

    internal val resultListLiveData by lazy { MutableLiveData<List<ResultPojo>>() }
    internal val resultLiveData by lazy { MutableLiveData<ResultPojo>() }
    internal val NetworkStatusLiveData by lazy { MutableLiveData<NetworkStatus>() }
    lateinit var appDatabase: AppDatabase
    lateinit var apiInterface: APIInterface
    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    fun setDBInstance(appDatabase: AppDatabase):NewsListViewmodel{
        this.appDatabase = appDatabase
        return this
    }

    fun setApiInterfaceInstance(apiInterface: APIInterface):NewsListViewmodel{
        this.apiInterface=apiInterface
        return this
    }

    fun callNewtWorkRepository(processScheduler: Scheduler = Schedulers.io(), androidScheduler: Scheduler = AndroidSchedulers.mainThread()) {
        val networkRepository = NetworkRepository(resultListLiveData, NetworkStatusLiveData)
        networkRepository.callNewsListApi(apiInterface, compositeDisposable, processScheduler, androidScheduler)
    }

     fun insertDatatoDB(results: List<ResultPojo>?) {
        results.let {
            DBRepository.insertResultsToDB(appDatabase, it!!) // switchMap will use DB results to filter user search operation
        }


    }


    fun resgisterSearchStringSubscription(stringObservable: Observable<String>): Observable<MutableList<ResultPojo>> {
        return stringObservable.debounce(300, TimeUnit.MILLISECONDS)/*.filter { it.isNotBlank() }*/.switchMap { getModifiedObservable(it) }
    }

    fun getModifiedObservable(text: String): Observable<MutableList<ResultPojo>>? {
        return Observable.create(object : ObservableOnSubscribe<MutableList<ResultPojo>> {
            override fun subscribe(e: ObservableEmitter<MutableList<ResultPojo>>) {
                val dbResultList = DBRepository.getAllResultsFromDB(appDatabase)
                val searchResultMatchList = mutableListOf<ResultPojo>()
                for (dbResults in dbResultList) {
                    if (text.isEmpty()) {
                        searchResultMatchList.clear()
                        searchResultMatchList.addAll(dbResultList)
                    } else {
                        dbResults.title.let {
                            if (it!!.contains(text, true)) {
                                searchResultMatchList.add(dbResults)
                            }
                        }
                    }
                }
                println("dbResultList Size->${dbResultList.size}")
                println("ResultList Size-> ${searchResultMatchList.size}")
                e.onNext(searchResultMatchList)
                e.onComplete()
            }

        }).subscribeOn(Schedulers.io())
    }

    fun getDataFromDB(appDatabase: AppDatabase): Boolean {
        if (DBRepository.getAllResultsFromDB(appDatabase).isNullOrEmpty())
            return false
        else
            resultListLiveData.postValue(DBRepository.getAllResultsFromDB(appDatabase) as ArrayList<ResultPojo>)
        return true
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}