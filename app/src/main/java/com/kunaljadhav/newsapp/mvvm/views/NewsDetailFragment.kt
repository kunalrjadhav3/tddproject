package com.kunaljadhav.newsapp.mvvm.views

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kunaljadhav.newsapp.R
import com.kunaljadhav.newsapp.mvvm.viewmodels.NewsListViewmodel
import com.kunaljadhav.newsapp.room.ResultPojo
import com.kunaljadhav.newsapp.utils.Utility
import kotlinx.android.synthetic.main.activity_detail.*

class NewsDetailFragment : Fragment() {

    private lateinit var newsListViewmodel: NewsListViewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        newsListViewmodel = ViewModelProviders.of(activity!!).get(NewsListViewmodel::class.java)
        setViewObservers()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_detail, container, false)
    }

    private fun setViewObservers() {
        newsListViewmodel.resultLiveData.observe(this, Observer {
            setDatatoView(it!!)
        })
    }

    fun setDatatoView(result: ResultPojo) {
        result.title?.let { title.text = it }
        result.abstract?.let { summary_content.text = it }
        news_image.setImageURI(result.img_url)
        setOnclickListeners(result)

    }

    private fun setOnclickListeners(result: ResultPojo) {
        full_story_link.setOnClickListener {
            val intent = Intent().apply {
                action = Intent.ACTION_VIEW
                data = result.story_url?.let { Uri.parse(it) }
            }
            if (intent.resolveActivity(activity!!.packageManager) != null && intent.data != null)
                startActivity(intent)
            else
                Utility.displaySnackbar(view!!, getString(R.string.error_msg))
        }

    }
}