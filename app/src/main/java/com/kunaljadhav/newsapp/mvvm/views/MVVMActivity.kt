package com.kunaljadhav.newsapp.mvvm.views

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.kunaljadhav.newsapp.R
import com.kunaljadhav.newsapp.utils.Constants_Kot
import com.kunaljadhav.newsapp.utils.FragmentsTag
import com.miguelcatalan.materialsearchview.MaterialSearchView
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_main.*


class MVVMActivity : AppCompatActivity(), NewsListFragment.Callback {

    val isTablet by lazy { resources.getBoolean(R.bool.isTablet) }

     var search_menuitem:MenuItem?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolBar)
        switchFragment(FragmentsTag.NEWSLIST_FRAG, null)
    }

    override fun onResume() {
        super.onResume()
        if (getfragmentInNewsListContainer() is NewsListFragment) {
            (getfragmentInNewsListContainer() as NewsListFragment).registerStringObservabletoObserver(SearhViewObservable())
        }
    }


    private fun SearhViewObservable(): Observable<String> {
        return Observable.create { e ->
            search_view.setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(text: String): Boolean {
                    e.onNext(text)
                    return false
                }

                override fun onQueryTextChange(text: String): Boolean {
                    e.onNext(text)
                    return false
                }
            })
        }

    }

    /**
     * @fragmentTag holds either @NewsListFragment/@NewsDetailFragment
     * @bundle holds data to be passed to @NewsListFragment/@NewsDetailFragment
     */
    fun switchFragment(fragmentTag: FragmentsTag, bundle: Bundle?) {
        var tag: String
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        var fragment = when (fragmentTag) {
            FragmentsTag.NEWSLIST_FRAG -> NewsListFragment()
            FragmentsTag.NEWSDETAIL_FRAG -> NewsDetailFragment()
        }

        tag = fragment.javaClass.simpleName
        bundle?.let { fragment.arguments = bundle }
        when (isTablet) {
            true -> { // its a Tablet
                if (tag.equals(Constants_Kot.NEWSLISTFRAG))
                    fragmentTransaction.replace(R.id.newsListfragment_container, fragment, tag)
                else{
                    fragmentTransaction.replace(R.id.newsdetailfragment_container, fragment, tag)
                }
            }
            false -> { //its a Phone
                fragmentTransaction.replace(R.id.newsListfragment_container, fragment, tag)
                if (tag.equals(Constants_Kot.NEWSDETAILFRAG)){
                    // add NewsDetailFragment to backstack
                     fragmentTransaction.addToBackStack(tag)
                    search_menuitem?.let { it.setVisible(false) } // hide toolbar searchView on loading @NewsListFragment

                }
            }
        }
        fragmentTransaction.commit()
    }

    override fun activityCallback() {
        if (getfragmentInNewsdetailContainer() == null) // NewsDetailFragment doesn't exist
            switchFragment(FragmentsTag.NEWSDETAIL_FRAG, null) // add it to container
    }


    fun getfragmentInNewsdetailContainer(): Fragment? {
        return supportFragmentManager.findFragmentById(R.id.newsdetailfragment_container)
    }

    fun getfragmentInNewsListContainer(): Fragment {
        return supportFragmentManager.findFragmentById(R.id.newsListfragment_container)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main_activity_kot, menu)
        search_menuitem = menu.findItem(R.id.action_search)
        search_view.setMenuItem(search_menuitem)
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        search_menuitem?.let { it.setVisible(true) } // show toolbar searchView on loading @NewsListFragment
    }


}
