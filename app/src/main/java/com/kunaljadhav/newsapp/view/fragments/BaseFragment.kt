package com.kunaljadhav.newsapp.view.fragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v4.app.Fragment
import com.kunaljadhav.newsapp.R

abstract class BaseFragment : Fragment() {
    lateinit var receiver: BroadcastReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity!!.registerReceiver(initInternetReceiver(), IntentFilter(getString(R.string.internetIntentFilter)))
    }

    private fun initInternetReceiver(): BroadcastReceiver {
        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetwork = connMgr.getActiveNetworkInfo()
                if (activeNetwork != null) {
                    if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                        checkInternetconnect(true)
                    }
                } else
                    checkInternetconnect(false)
            }
        }
        return receiver
    }

    internal abstract fun checkInternetconnect(isConnected: Boolean)

    override fun onDestroy() {
        activity!!.unregisterReceiver(receiver)
        super.onDestroy()
    }
}
