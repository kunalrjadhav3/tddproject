package com.kunaljadhav.newsapp.view.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.facebook.drawee.view.SimpleDraweeView
import com.kunaljadhav.newsapp.R
import com.kunaljadhav.newsapp.room.ResultPojo

class NewslistAdapter(var resultList: ArrayList<ResultPojo>, val mListener: ItemClickListener) : RecyclerView.Adapter<NewslistAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item_news, parent, false);
        return MyViewHolder(itemView);
    }

    /**
     *  @ItemClickListener interface for data communication with NewsListFragment
     */
    interface ItemClickListener {
        fun onitemClick(result: ResultPojo)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var result: ResultPojo = resultList.get(position);
        result.title?.let { holder.newsTitle.text = result.title }
         holder.newsItemImage.setImageURI(result.thumb_url)
        //result.multimedia?.get(0)?.url?.let { holder.newsItemImage.setImageURI(result.multimedia!!.get(0)!!.url) }
        holder.container.setOnClickListener { mListener.onitemClick(result) }
    }


    override fun getItemCount(): Int {
        return resultList.size
    }


    inner class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var container: LinearLayout = itemView!!.findViewById(R.id.container)
        var newsTitle: TextView = itemView!!.findViewById(R.id.news_title)
        var newsItemImage: SimpleDraweeView = itemView!!.findViewById(R.id.news_item_image)
    }

    internal fun getItemList()=resultList


}