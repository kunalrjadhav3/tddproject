package com.kunaljadhav.newsapp

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.facebook.drawee.backends.pipeline.Fresco
import com.kunaljadhav.newsapp.di.components.ApplicationComponent
import com.kunaljadhav.newsapp.di.components.DaggerApplicationComponent
import com.kunaljadhav.newsapp.di.modules.ApplicationModule
import io.fabric.sdk.android.Fabric


class MainApp : Application() {


    companion object {
        lateinit var applicationComponent: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this) // init ImageLoader
        Fabric.with(this, Crashlytics()) //init for Frabic beta release use in CI & Crashlytics
        applicationComponent = initDagger()

    }

    /**
     * initialize dependencies with Singleton scope
     */
    private fun initDagger(): ApplicationComponent {
        var applicationComponent = DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this))
                .build()
        applicationComponent.inject(this)
        return applicationComponent
    }


}