package com.kunaljadhav.newsapp.model

import com.google.gson.annotations.SerializedName

data class Result(
        @SerializedName("abstract")
        var `abstract`: String?,
        @SerializedName("byline")
        var byline: String?,
        @SerializedName("created_date")
        var createdDate: String?,
        @SerializedName("des_facet")
        var desFacet: Array<String?>?,
        @SerializedName("geo_facet")
        var geoFacet: Array<String?>?,
        @SerializedName("item_type")
        var itemType: String?,
        @SerializedName("kicker")
        var kicker: String?,
        @SerializedName("material_type_facet")
        var materialTypeFacet: String?,
        @SerializedName("multimedia")
        var multimedia: Array<Multimedia?>?,
        @SerializedName("org_facet")
        var orgFacet: Array<String?>?,
        @SerializedName("per_facet")
        var perFacet: Array<String?>?,
        @SerializedName("published_date")
        var publishedDate: String?,
        @SerializedName("section")
        var section: String?,
        @SerializedName("subsection")
        var subsection: String?,
        @SerializedName("title")
        var title: String?,
        @SerializedName("updated_date")
        var updatedDate: String?,
        @SerializedName("url")
        var url: String?
)