package com.kunaljadhav.newsapp.model

import com.google.gson.annotations.SerializedName

data class NewsDataResponse(
        @SerializedName("copyright")
        var copyright: String?,
        @SerializedName("last_updated")
        var lastUpdated: String?,
        @SerializedName("num_results")
        var numResults: Int?,
        @SerializedName("results")
        var results: List<Result?>?,
        @SerializedName("section")
        var section: String?,
        @SerializedName("status")
        var status: String?
)