package com.kunaljadhav.newsapp.mvp.contracts

import com.kunaljadhav.newsapp.retrofit.APIInterface
import com.kunaljadhav.newsapp.room.ResultPojo
import io.reactivex.Scheduler

/**
 * 1-1 mapping of NewsListFragment(view) and NewsListPresenter(presenter)
 */
open class NewsListContract {

    interface view : BaseContract.baseView {
        fun loadNewsData(result: List<ResultPojo>?)
    }

    interface presenter : BaseContract.basepresenter<view> {
        fun callNewsListApi(apiInterface: APIInterface)
        fun getCachedData(): List<ResultPojo>?
        fun setSchedulers(processScheduler : Scheduler, androidScheduler: Scheduler)
    }
}