package com.kunaljadhav.newsapp.mvp.views

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kunaljadhav.newsapp.R
import com.kunaljadhav.newsapp.utils.Constants_Kot
import com.kunaljadhav.newsapp.utils.Utility
import kotlinx.android.synthetic.main.activity_detail.*

class NewsDetailFragment : Fragment() {



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setDatatoView(arguments!!)
    }

    fun setDatatoView(bundle: Bundle) {
        bundle.getString(Constants_Kot.TITLE)?.let {title.text = bundle.getString(Constants_Kot.TITLE)  }
        bundle.getString(Constants_Kot.ABSTRACT)?.let { summary_content.text = bundle.getString(Constants_Kot.ABSTRACT) }
        news_image.setImageURI(bundle.getString(Constants_Kot.URL))
        setOnclickListeners(bundle)

    }

    private fun setOnclickListeners(bundle: Bundle) {
        full_story_link.setOnClickListener {
            val intent = Intent().apply {
                action = Intent.ACTION_VIEW
                data = bundle.getString(Constants_Kot.STORYURL)?.let { Uri.parse(bundle.getString(Constants_Kot.STORYURL))}
            }
            if (intent.resolveActivity(activity!!.packageManager) != null && intent.data!=null)
                startActivity(intent)
            else
                Utility.displaySnackbar(view!!, getString(R.string.error_msg))
        }

    }


}
