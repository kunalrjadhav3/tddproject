package com.kunaljadhav.newsapp.mvp.presenters

import com.kunaljadhav.newsapp.model.NewsDataResponse
import com.kunaljadhav.newsapp.model.Result
import com.kunaljadhav.newsapp.mvp.contracts.NewsListContract
import com.kunaljadhav.newsapp.retrofit.APIInterface
import com.kunaljadhav.newsapp.room.ResultPojo
import com.kunaljadhav.newsapp.utils.Constants_Kot
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable

class NewsListPresenter : NewsListContract.presenter {


    private lateinit var view: NewsListContract.view
    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }
    private var resultList: List<ResultPojo>? = null

    lateinit var processScheduler : Scheduler
    lateinit var androidScheduler:Scheduler

    override fun getCachedData() = resultList

    override fun attachView(view: NewsListContract.view) {
        this.view = view
    }

    override fun setSchedulers(processScheduler : Scheduler, androidScheduler: Scheduler) {
        this.processScheduler=processScheduler
        this.androidScheduler=androidScheduler
    }

    override fun callNewsListApi(apiInterface: APIInterface) {
        compositeDisposable.add(apiInterface.getNewsData().observeOn(androidScheduler)
                .subscribeOn(processScheduler)
                .subscribe(this::handleResponse, this::handleError))

    }

    private fun handleResponse(newsDataResponse: NewsDataResponse) {
        if (newsDataResponse.status.toString().equals(Constants_Kot.statusOK) && !newsDataResponse.results.isNullOrEmpty()) {
            resultList = parseNetworkResponse(newsDataResponse.results as List<Result>)
            view.loadNewsData(resultList)
        } else {
            view.onError()
        }
    }

    private fun handleError(error: Throwable) {
        error.printStackTrace()
        view.onError()

    }

    private fun parseNetworkResponse(resultsList: List<Result>):List<ResultPojo> {
        val resultTableList = mutableListOf<ResultPojo>()
        for (result in resultsList)
            resultTableList.add(ResultPojo(0, result.title,
                    result.multimedia?.get(0)?.url?.let { it },
                    result.abstract, result.multimedia?.let { it.get(2)?.let { it.url?.let { it } } },
                    result.url))
        return resultTableList
    }


    override fun onViewDestroy() {
        compositeDisposable.clear()
    }
}