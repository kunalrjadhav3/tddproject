package com.kunaljadhav.newsapp.mvp.views

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.kunaljadhav.newsapp.R
import com.kunaljadhav.newsapp.room.ResultPojo
import com.kunaljadhav.newsapp.utils.Constants_Kot
import com.kunaljadhav.newsapp.utils.FragmentsTag
import kotlinx.android.synthetic.main.activity_main.*

class MVPActivity : AppCompatActivity(), NewsListFragment.PassDataToActivity {
    val isTablet by lazy { resources.getBoolean(R.bool.isTablet) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolBar)
        switchFragment(FragmentsTag.NEWSLIST_FRAG, null)


    }


    /**
     * @fragmentTag holds either @NewsListFragment/@NewsDetailFragment
     * @bundle holds data to be passed to @NewsListFragment/@NewsDetailFragment
     */
    fun switchFragment(fragmentTag: FragmentsTag, bundle: Bundle?) {
        var tag: String
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        var fragment=when (fragmentTag) {
            FragmentsTag.NEWSLIST_FRAG -> NewsListFragment()
            FragmentsTag.NEWSDETAIL_FRAG -> NewsDetailFragment()
        }

        tag=fragment.javaClass.simpleName
        bundle?.let { fragment.arguments = bundle }
        when(isTablet){
            true->{ // its a Tablet
                if (tag.equals(Constants_Kot.NEWSLISTFRAG))
                    fragmentTransaction.replace(R.id.newsListfragment_container, fragment, tag)
                else
                    fragmentTransaction.replace(R.id.newsdetailfragment_container, fragment, tag)
            }
            false->{ //its a Phone
                fragmentTransaction.replace(R.id.newsListfragment_container, fragment, tag)
                if (tag.equals(Constants_Kot.NEWSDETAILFRAG)) // add NewsDetailFragment to backstack
                    fragmentTransaction.addToBackStack(tag)
            }
        }
        fragmentTransaction.commit()
    }

    /**
     * @result  - data to be passed from @NewsListFragment to @NewsDetailFragment
     */
    override fun newsListData(result: ResultPojo) {
        var bundle = Bundle().apply {
            putString(Constants_Kot.ABSTRACT, result.abstract)
            putString(Constants_Kot.TITLE, result.title)
            putString(Constants_Kot.STORYURL, result.story_url)
            putString(Constants_Kot.URL,result.img_url)
          //  result.multimedia?.get(2)?.url?.let { putString(Constants_Kot.URL, result.multimedia!!.get(2)!!.url) }
        }
        if (isTablet and (getNewsdetailfragment() is NewsDetailFragment))   // if Tablet and NewsDetailFragment already exists
            (getNewsdetailfragment() as NewsDetailFragment).setDatatoView(bundle) // set Data to NewsDetailFragment
        else
            switchFragment(FragmentsTag.NEWSDETAIL_FRAG, bundle) // NewsDetailFragment doesn't exists, add it to container

    }

    fun getNewsdetailfragment(): Fragment? {
        return supportFragmentManager.findFragmentById(R.id.newsdetailfragment_container)
    }





}