package com.kunaljadhav.newsapp.mvp.contracts

class BaseContract {
    interface basepresenter<T> {
        fun attachView(view: T)
        fun onViewDestroy()
    }

    interface baseView {
        fun onError()
    }

}