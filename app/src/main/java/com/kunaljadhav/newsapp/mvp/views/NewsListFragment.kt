package com.kunaljadhav.newsapp.mvp.views

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kunaljadhav.newsapp.MainApp
import com.kunaljadhav.newsapp.R
import com.kunaljadhav.newsapp.di.components.DaggerFragmentComponent
import com.kunaljadhav.newsapp.di.components.FragmentComponent
import com.kunaljadhav.newsapp.di.modules.FragmentModule
import com.kunaljadhav.newsapp.mvp.contracts.NewsListContract
import com.kunaljadhav.newsapp.retrofit.APIInterface
import com.kunaljadhav.newsapp.room.ResultPojo
import com.kunaljadhav.newsapp.utils.Utility
import com.kunaljadhav.newsapp.view.adapters.NewslistAdapter
import com.kunaljadhav.newsapp.view.fragments.BaseFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_news_list.*
import javax.inject.Inject

class NewsListFragment : BaseFragment(), NewsListContract.view {

    private var fragmentComponent: FragmentComponent? = null
    @Inject
    lateinit var presenter: NewsListContract.presenter
    @Inject
    lateinit var apiInterface: APIInterface

    private lateinit var passDataToActivity: PassDataToActivity


    override fun checkInternetconnect(isConnected: Boolean) {
        if (isConnected && presenter.getCachedData()==null){ // if internet available and data not loaded, call API
            progressBar.visibility= View.VISIBLE
            presenter.setSchedulers(processScheduler = Schedulers.io(),androidScheduler = AndroidSchedulers.mainThread())
            presenter.callNewsListApi(apiInterface)
        }
        if (!isConnected && presenter.getCachedData()==null) //if internet unavailable and data not loaded, show message
            Utility.displaySnackbar(view!!,getString(R.string.noInternet_msg))

    }
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MVPActivity) {
            passDataToActivity = context
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDependency()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_news_list, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.getCachedData()?.let { initRecyclerView(presenter.getCachedData()!!) }

        if(presenter.getCachedData()==null)
            presenter.attachView(this)

    }

    /**
     * @PassDataToActivity interface for data communication with @MainActivity
     */
    interface PassDataToActivity {
        fun newsListData(result: ResultPojo)
    }

    /**
     * initialize dependencies with @CustomScope
     */
    private fun initDependency() {
        fragmentComponent = DaggerFragmentComponent.builder().applicationComponent(MainApp.applicationComponent)
                .fragmentModule(FragmentModule())
                .build()
        fragmentComponent!!.inject(this)


    }



    override fun loadNewsData(result: List<ResultPojo>?) {
        progressBar.visibility= View.GONE
        initRecyclerView(result!!)
    }

    override fun onError() {
        progressBar.visibility= View.GONE
        Utility.displaySnackbar(view!!, getString(R.string.error_noData))
    }

    private fun initRecyclerView(resultList: List<ResultPojo>) {
        var adapter = NewslistAdapter(resultList as ArrayList<ResultPojo>, object : NewslistAdapter.ItemClickListener {
            override fun onitemClick(result: ResultPojo) {
                passDataToActivity.newsListData(result)
            }
        })

        var mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(getContext())
        recycler.setLayoutManager(mLayoutManager)
        recycler.setItemAnimator(DefaultItemAnimator())
        recycler.setAdapter(adapter)
    }

    override fun onDestroy() {
        super.onDestroy()
        fragmentComponent = null // set @CustomScope dependencies to null
        presenter.onViewDestroy() // Dispose presenter subscribers
    }
}
