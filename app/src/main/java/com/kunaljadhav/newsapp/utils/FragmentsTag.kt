package com.kunaljadhav.newsapp.utils

/**
 * @NEWSLIST_FRAG refers to NewsListFragment
 * @NEWSDETAIL_FRAG refers to NewsDetailFragment
 */
enum class FragmentsTag{
    NEWSDETAIL_FRAG, NEWSLIST_FRAG
}