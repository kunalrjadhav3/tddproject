package com.kunaljadhav.newsapp.utils

import android.support.design.widget.Snackbar
import android.view.View

/**
 * Singleton class
 */
object Utility {

    fun displaySnackbar(rootView: View, message: String) {
        val snackbar = Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT)
        snackbar.show()
    }
}