package com.kunaljadhav.newsapp.utils

sealed class NetworkStatus {
    class Loading() : NetworkStatus()
    class Loaded() : NetworkStatus()
    class Error(val message: String) : NetworkStatus()
    class Exception(val exception: Throwable) : NetworkStatus()
}