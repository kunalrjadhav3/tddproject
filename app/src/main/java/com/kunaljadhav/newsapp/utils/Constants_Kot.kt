package com.kunaljadhav.newsapp.utils

class Constants_Kot {
    companion object {
        const val  NEWSLISTFRAG = "NewsListFragment";
        const val NEWSDETAILFRAG = "NewsDetailFragment";
        const val BASE_URL = "http://api.myjson.com";
        const val ABSTRACT="abstract"
        const val TITLE="title"
        const val URL="url"
        const val STORYURL ="storyUrl"
        const val statusOK="OK"
        const val statusERROR="ERROR"
        const val error_noData="Technical error, please try later"
        const val DBName="NewsAppDB.db"
    }
}