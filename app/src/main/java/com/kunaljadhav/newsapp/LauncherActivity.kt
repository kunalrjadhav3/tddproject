package com.kunaljadhav.newsapp

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.kunaljadhav.newsapp.di.components.ActivityComponent
import com.kunaljadhav.newsapp.di.components.DaggerActivityComponent
import com.kunaljadhav.newsapp.mvp.views.MVPActivity
import com.kunaljadhav.newsapp.mvvm.views.MVVMActivity
import com.kunaljadhav.newsapp.repository.DBRepository
import com.kunaljadhav.newsapp.room.AppDatabase
import kotlinx.android.synthetic.main.activity_launcher.*
import javax.inject.Inject

class LauncherActivity : AppCompatActivity() {

    @Inject
    lateinit var appDatabase: AppDatabase

    private var activityComponent: ActivityComponent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)
        initDependency()

        btn_mvp.setOnClickListener {
            startActivity(Intent(this, MVPActivity::class.java))
        }
        btn_mvvm.setOnClickListener {
            startActivity(Intent(this, MVVMActivity::class.java))
        }
    }



    /**
     * initialize dependencies with @CustomScope
     */
    private fun initDependency() {
        activityComponent = DaggerActivityComponent.builder().applicationComponent(MainApp.applicationComponent).build()
        activityComponent!!.inject(this)
    }


    override fun onBackPressed() {
        super.onBackPressed()
        DBRepository.clearAllDBTables(appDatabase)
    }

}
