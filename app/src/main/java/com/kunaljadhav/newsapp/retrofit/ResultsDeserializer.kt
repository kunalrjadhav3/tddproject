package com.kunaljadhav.newsapp.retrofit

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.kunaljadhav.newsapp.model.Multimedia
import com.kunaljadhav.newsapp.model.Result
import java.lang.reflect.Type

class ResultsDeserializer : JsonDeserializer<Result> {
    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext): Result {
        var per_facet: Array<String?>? = null
        var des_facet: Array<String?>? = null
        var geo_facet: Array<String?>? = null
        var org_facet: Array<String?>? = null
        var multimedia: Array<Multimedia?>? = null
        if (json.asJsonObject.get("per_facet").isJsonArray)
            per_facet = context.deserialize(json.asJsonObject.get("per_facet").asJsonArray, Array<String>::class.java) as Array<String?>
        if (json.asJsonObject.get("des_facet").isJsonArray)
            des_facet = context.deserialize(json.asJsonObject.get("des_facet").asJsonArray, Array<String>::class.java) as Array<String?>
        if (json.asJsonObject.get("geo_facet").isJsonArray)
            geo_facet = context.deserialize(json.asJsonObject.get("geo_facet").asJsonArray, Array<String>::class.java) as Array<String?>
        if (json.asJsonObject.get("org_facet").isJsonArray)
            org_facet = context.deserialize(json.asJsonObject.get("org_facet").asJsonArray, Array<String>::class.java) as Array<String?>
        if (json.asJsonObject.get("multimedia").isJsonArray)
            multimedia = context.deserialize(json.asJsonObject.get("multimedia").asJsonArray, Array<Multimedia>::class.java)

        return Result(json.asJsonObject.get("abstract").asString,
                json.asJsonObject.get("byline").asString,
                json.asJsonObject.get("created_date").asString,
                desFacet = des_facet,
                geoFacet = geo_facet,
                itemType = json.asJsonObject.get("item_type").asString,
                kicker = json.asJsonObject.get("kicker").asString,
                materialTypeFacet = json.asJsonObject.get("material_type_facet").asString,
                multimedia = multimedia,
                orgFacet = org_facet,
                perFacet = per_facet,
                publishedDate = json.asJsonObject.get("published_date").asString,
                section = json.asJsonObject.get("section").asString,
                subsection = json.asJsonObject.get("subsection").asString,
                title = json.asJsonObject.get("title").asString,
                updatedDate = json.asJsonObject.get("updated_date").asString,
                url = json.asJsonObject.get("url").asString)
    }
}