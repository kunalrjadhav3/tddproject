package com.kunaljadhav.newsapp.retrofit

import com.kunaljadhav.newsapp.model.NewsDataResponse
import io.reactivex.Single
import retrofit2.http.GET

interface APIInterface {
    @GET("/bins/nl6jh")
    fun getNewsData(): Single<NewsDataResponse>
}