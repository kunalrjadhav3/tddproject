package com.kunaljadhav.newsapp.room

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "ResultPojo")
data class ResultPojo(@PrimaryKey(autoGenerate = true) var id:Long,
                       @ColumnInfo(name = "news_title") var title:String?,
                      var thumb_url:String?,
                       var abstract:String?,
                       var img_url:String?,
                      var story_url:String?){
    public constructor() : this(0,"","","","","")
}