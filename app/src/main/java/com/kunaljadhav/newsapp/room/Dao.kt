package com.kunaljadhav.newsapp.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
public interface Dao {

    @Query("SELECT * FROM ResultPojo")
    fun getAll(): List<ResultPojo>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    fun insertAll(results: List<ResultPojo>)

}