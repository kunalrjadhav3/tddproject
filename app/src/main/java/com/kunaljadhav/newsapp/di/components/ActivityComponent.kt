package com.kunaljadhav.newsapp.di.components

import com.kunaljadhav.newsapp.LauncherActivity
import com.kunaljadhav.newsapp.di.scopes.CustomScope
import dagger.Component

@CustomScope
@Component(dependencies = arrayOf(ApplicationComponent::class))
interface ActivityComponent {
    fun inject(launcherActivity: LauncherActivity)
}