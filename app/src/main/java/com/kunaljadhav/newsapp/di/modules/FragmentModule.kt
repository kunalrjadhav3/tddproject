package com.kunaljadhav.newsapp.di.modules

import com.kunaljadhav.newsapp.di.scopes.CustomScope
import com.kunaljadhav.newsapp.mvp.contracts.NewsListContract
import com.kunaljadhav.newsapp.mvp.presenters.NewsListPresenter
import dagger.Module
import dagger.Provides

@Module
class FragmentModule {
    @Provides
    @CustomScope
    fun getPresenter(): NewsListContract.presenter {
        return NewsListPresenter()
    }
}