package com.kunaljadhav.newsapp.di.components

import com.kunaljadhav.newsapp.di.modules.FragmentModule
import com.kunaljadhav.newsapp.di.scopes.CustomScope
import com.kunaljadhav.newsapp.mvp.views.NewsListFragment
import dagger.Component

@CustomScope
@Component(dependencies = arrayOf(ApplicationComponent::class),modules = arrayOf(FragmentModule::class))
interface FragmentComponent {
    fun inject(newsListFragment: NewsListFragment)
    fun inject(newsListFragment: com.kunaljadhav.newsapp.mvvm.views.NewsListFragment)
}