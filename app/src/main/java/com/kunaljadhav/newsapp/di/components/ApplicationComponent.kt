package com.kunaljadhav.newsapp.di.components

import com.kunaljadhav.newsapp.MainApp
import com.kunaljadhav.newsapp.di.modules.ApplicationModule
import com.kunaljadhav.newsapp.retrofit.APIInterface
import com.kunaljadhav.newsapp.room.AppDatabase
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {
    fun inject(mainApp: MainApp)
    fun apiInterface(): APIInterface // Explicit declaration, to make APIInterface dependency available to sub components (FragmentComponent)
    fun appDatabase(): AppDatabase // Explicit declaration, to make AppDatabase dependency available to sub components (FragmentComponent)
}