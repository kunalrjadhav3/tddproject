package com.kunaljadhav.newsapp.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import com.google.gson.GsonBuilder
import com.kunaljadhav.newsapp.model.Result
import com.kunaljadhav.newsapp.retrofit.APIInterface
import com.kunaljadhav.newsapp.retrofit.ResultsDeserializer
import com.kunaljadhav.newsapp.room.AppDatabase
import com.kunaljadhav.newsapp.utils.Constants_Kot
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class ApplicationModule(val context: Context) {

    @Provides
    @Singleton
    fun getApiInterface(): APIInterface {
        val customDeserializer =
                GsonBuilder().registerTypeAdapter(Result::class.java, ResultsDeserializer()).create()
        val retrofit = Retrofit.Builder().baseUrl(Constants_Kot.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(customDeserializer))
                .build()
        return retrofit.create(APIInterface::class.java)
    }
@Provides
@Singleton
    fun getAppDatabase():AppDatabase{
        return Room.databaseBuilder(context, AppDatabase::class.java, Constants_Kot.DBName)
                .build()
    }


}