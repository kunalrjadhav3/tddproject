package com.kunaljadhav.newsapp.repository

import android.arch.lifecycle.MutableLiveData
import com.kunaljadhav.newsapp.model.NewsDataResponse
import com.kunaljadhav.newsapp.model.Result
import com.kunaljadhav.newsapp.retrofit.APIInterface
import com.kunaljadhav.newsapp.room.ResultPojo
import com.kunaljadhav.newsapp.utils.Constants_Kot
import com.kunaljadhav.newsapp.utils.NetworkStatus
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable

class NetworkRepository(val resultListLiveData: MutableLiveData<List<ResultPojo>>, val NetworkStatusLiveData: MutableLiveData<NetworkStatus>) {


    fun callNewsListApi(apiInterface: APIInterface, compositeDisposable: CompositeDisposable, processScheduler: Scheduler, androidScheduler: Scheduler) {
        NetworkStatusLiveData.postValue(NetworkStatus.Loading())
        compositeDisposable.add(apiInterface.getNewsData().observeOn(androidScheduler)
                .subscribeOn(processScheduler)
                .subscribe(this::handleResponse, this::handleError))

    }

    private fun handleResponse(newsDataResponse: NewsDataResponse) {
        if (newsDataResponse.status.toString().equals(Constants_Kot.statusOK) && !newsDataResponse.results.isNullOrEmpty()) {
            resultListLiveData.postValue(parseNetworkResponse(newsDataResponse.results as List<Result>))
            NetworkStatusLiveData.postValue(NetworkStatus.Loaded())

        } else {
            NetworkStatusLiveData.postValue(NetworkStatus.Error(Constants_Kot.error_noData))
        }
    }

    private fun parseNetworkResponse(resultsList: List<Result>):List<ResultPojo> {
        val resultTableList = mutableListOf<ResultPojo>()
        for (result in resultsList!!)
            resultTableList.add(ResultPojo(0, result.title,
                    result.multimedia?.get(0)?.url?.let { it },
                    result.abstract, result.multimedia?.let { it.get(2)?.let { it.url?.let { it } } },
                    result.url))
        return resultTableList
    }

    private fun handleError(error: Throwable) {
        NetworkStatusLiveData.postValue(NetworkStatus.Exception(error))
        error.printStackTrace()
    }
}