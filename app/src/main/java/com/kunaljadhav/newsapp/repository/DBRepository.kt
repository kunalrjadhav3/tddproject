package com.kunaljadhav.newsapp.repository

import com.kunaljadhav.newsapp.room.AppDatabase
import com.kunaljadhav.newsapp.room.ResultPojo
import kotlinx.coroutines.*

/*
* runBlocking coroutine -> use it if you want program execution to be synchronous
* GlobalScope coroutine -> use it if you want program execution to be asynchronous
* */
object DBRepository {
    fun insertResultsToDB(db: AppDatabase, resultList: List<ResultPojo>) {
        GlobalScope.launch { db.resultDao().insertAll(resultList) }
    }

    fun getAllResultsFromDB(db: AppDatabase): List<ResultPojo> {
        return runBlocking {
            withContext(Dispatchers.IO){ db.resultDao().getAll() }  }  // runBlocking is used, because we want getAllResultsFromDB() operation
                                                                      // to be finished first, before moving to next line of getAllResultsFromDB() caller code }
    }

    fun clearAllDBTables(db: AppDatabase) = GlobalScope.launch { db.clearAllTables() }

}