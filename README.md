#Name-
 News app
#Description-
 The project loads a list of news from open API.
#Features-
 Store news to app database, after loading it from API (achieved using Room DB with coroutines)

 Offline loading of news from app database, when internet is not available (achieved using Broadcast receiver)

 Search news by typing news title (achieved using RX kotlin with Filter and Switchmap operator)